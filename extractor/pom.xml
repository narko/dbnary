<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.getalp</groupId>
    <artifactId>dbnary</artifactId>
    <version>1.3-SNAPSHOT</version>
    <packaging>jar</packaging>

    <reporting>
        <plugins>
	    <plugin>
	        <groupId>org.apache.maven.plugins</groupId>
	        <artifactId>maven-javadoc-plugin</artifactId>
	        <version>2.10.4</version>
	        <configuration>
		    <failOnError>false</failOnError>
	            <tags>
		        <tag>
		            <name>uml.property</name>
		            <!-- The value X makes javadoc ignoring the tag -->
		            <placement>X</placement>
		        </tag>
		    </tags>
	        </configuration>
	    </plugin>
        </plugins>
    </reporting>
    
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <scalaBinaryVersion>2.12</scalaBinaryVersion>
    </properties>

    <name>Wiktionary Multilingual Data Extraction</name>
    <description>The dbnary project provides tools to extract lexical networks from wiktionary dumps in Bulgarian, Dutch, English, Finnish, French, German, Greek, Italian, Japanese, Polish, Portuguese, Russian, Serbo-Croat, Spanish, Swedish and Turkish. More languages to be added later.
    </description>
    <inceptionYear>2010</inceptionYear>
    <url>http://kaiko.getalp.org/about-dbnary</url>
    <licenses>
        <license>
            <name>LGPL 2.1</name>
            <url>http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <issueManagement>
        <system>bitbucket</system>
        <url>https://bitbucket.org/serasset/dbnary/issues</url>
    </issueManagement>

    <developers>
        <developer>
            <name>Gilles Sérasset</name>
        </developer>
    </developers>

    <organization>
        <name>GETALP</name>
        <url>http://www.getalp.org/</url>
    </organization>

    <distributionManagement>
        <repository>
            <id>getalp.repository</id>
            <name>Getalp Projects and third party repository</name>
            <url>scpexe://getalp.imag.fr/opt/www/m2</url>
        </repository>
    </distributionManagement>
    <scm>
        <connection>scm:git:ssh://git@bitbucket.org/serasset/dbnary.git</connection>
        <developerConnection>scm:git:ssh://git@bitbucket.org/serasset/dbnary.git</developerConnection>
        <url>https://bitbucket.org/serasset/dbnary.git</url>
    </scm>
    <!--scm>
        <connection>scm:svn:svn://scm.forge.imag.fr/var/lib/gforge/chroot/scmrepos/svn/dbnary/trunk</connection>
        <developerConnection>scm:svn:svn+ssh://scm.forge.imag.fr/var/lib/gforge/chroot/scmrepos/svn/dbnary/trunk
        </developerConnection>
        <url>https://forge.imag.fr/scm/browser.php?group_id=362</url>
    </scm-->

    <dependencies>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>1.7.7</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>1.7.7</version>
        </dependency>
        <dependency>
            <groupId>org.codehaus.woodstox</groupId>
            <artifactId>woodstox-core-lgpl</artifactId>
            <version>4.0.8</version>
            <type>jar</type>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>commons-cli</groupId>
            <artifactId>commons-cli</artifactId>
            <version>1.2</version>
            <type>jar</type>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.jena</groupId>
            <artifactId>apache-jena-libs</artifactId>
            <type>pom</type>
            <version>2.12.1</version>
            <exclusions>
                <exclusion>
                    <groupId>org.slf4j</groupId>
                    <artifactId>slf4j-log4j12</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>log4j</groupId>
                    <artifactId>log4j</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>commons-net</groupId>
            <artifactId>commons-net</artifactId>
            <version>3.1</version>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-compress</artifactId>
            <version>1.12</version>
        </dependency>
        <dependency>
            <groupId>info.bliki.wiki</groupId>
            <artifactId>bliki-core</artifactId>
            <!--version>3.0.20-SNAPSHOT</version-->
            <version>3.1.1-SNAPSHOT</version>
        </dependency>
        <dependency>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-core-lgpl</artifactId>
            <version>1.9.12</version>
        </dependency>
        <dependency>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-mapper-lgpl</artifactId>
            <version>1.9.12</version>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>3.5</version>
        </dependency>
        <!-- WARN: this dependency is also used by JENA, check that the required version has not evolved since. -->
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpclient</artifactId>
            <version>4.2.6</version>
        </dependency>
        <!--dependency>
            <groupId>de.tudarmstadt.ukp.omegawikiapi</groupId>
            <artifactId>de.tudarmstadt.ukp.omegawikiapi-asl</artifactId>
            <version>0.2.0</version>
        </dependency-->
        <dependency>
            <groupId>org.sweble.wikitext</groupId>
            <artifactId>swc-engine</artifactId>
            <version>2.0.1-SNAPSHOT</version>
        </dependency>
        <dependency>
            <groupId>org.sweble.wikitext</groupId>
            <artifactId>swc-example-xpath</artifactId>
            <version>2.0.1-SNAPSHOT</version>
        </dependency>
        <dependency>
            <groupId>org.getalp.dbnary</groupId>
            <artifactId>ontology</artifactId>
            <version>1.5-SNAPSHOT</version>
        </dependency>
        <dependency>
            <groupId>net.sf.ehcache</groupId>
            <artifactId>ehcache</artifactId>
            <version>2.8.4</version>
        </dependency>
        <dependency>
            <!-- jsoup HTML parser library @ http://jsoup.org/ -->
            <groupId>org.jsoup</groupId>
            <artifactId>jsoup</artifactId>
            <version>1.8.1</version>
        </dependency>
        <dependency>
            <groupId>org.scala-lang</groupId>
            <artifactId>scala-library</artifactId>
            <version>2.12.1</version>
        </dependency>
        <!-- taken from: http://github.com/scala/scala-module-dependency-sample -->
        <dependency>
            <groupId>org.scala-lang.modules</groupId>
            <artifactId>scala-xml_${scalaBinaryVersion}</artifactId>
            <version>1.0.6</version>
        </dependency>
        <dependency>
            <groupId>org.scala-lang.modules</groupId>
            <artifactId>scala-parser-combinators_${scalaBinaryVersion}</artifactId>
            <version>1.0.5</version>
        </dependency>
        <!--dependency>
            <groupId>org.scala-lang.modules</groupId>
            <artifactId>scala-swing_${scalaBinaryVersion}</artifactId>
            <version>1.0.2</version>
        </dependency-->
        <!-- Small wrapper around SLF4j in scala -->
        <dependency>
            <groupId>org.clapper</groupId>
            <artifactId>grizzled-slf4j_2.12</artifactId>
            <version>1.3.0</version>
        </dependency>
        <!-- parboiled2 a scala PEG parser -->
        <!--dependency>
            <groupId>org.parboiled</groupId>
            <artifactId>parboiled_2.11</artifactId>
            <version>2.1.3</version>
        </dependency-->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.9</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.scalatest</groupId>
            <artifactId>scalatest_2.12</artifactId>
            <version>3.0.1</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <extensions>
            <extension>
                <groupId>org.apache.maven.wagon</groupId>
                <artifactId>wagon-ssh</artifactId>
                <version>2.6</version>
            </extension>
            <extension>
                <groupId>org.apache.maven.wagon</groupId>
                <artifactId>wagon-ssh-external</artifactId>
                <version>2.6</version>
            </extension>
        </extensions>
        <plugins>
	    <plugin>
	        <groupId>org.apache.maven.plugins</groupId>
	        <artifactId>maven-javadoc-plugin</artifactId>
	        <version>2.10.4</version>
	        <configuration>
		    <tags>
		        <tag>
			    <name>uml.property</name>
			    <!-- The value X makes javadoc ignoring the tag -->
			    <placement>X</placement>
		        </tag>
		    </tags>
	        </configuration>
	    </plugin>
            <plugin>
                <groupId>net.alchim31.maven</groupId>
                <artifactId>scala-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <id>scala-compile-first</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>add-source</goal>
                            <goal>compile</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>scala-test-compile</id>
                        <phase>process-test-resources</phase>
                        <goals>
                            <goal>testCompile</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>compile</phase>
                        <goals>
                            <goal>compile</goal>
                        </goals>
                    </execution>
                </executions><!-- TODO: Change plugins versions to current stable versions -->
                <configuration>
                    <fork>true</fork>
                    <source>7</source>
                    <target>7</target>
                </configuration>
            </plugin>
            <plugin>
                <!-- NOTE: We don't need a groupId specification because the group is
                    org.apache.maven.plugins ...which is assumed by default. -->
                <artifactId>maven-assembly-plugin</artifactId>
                <version>2.4</version>
                <configuration>
                    <descriptorRefs>
                        <descriptorRef>jar-with-dependencies</descriptorRef>
                    </descriptorRefs>
                </configuration>
                <executions>
                    <execution>
                        <id>make-assembly</id>
                        <!-- this is used for inheritance merges -->
                        <phase>package</phase>
                        <!-- bind to the packaging phase -->
                        <goals>
                            <goal>single</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>2.5</version>
                <configuration>
                    <tagBase>svn+ssh://scm.forge.imag.fr/var/lib/gforge/chroot/scmrepos/svn/dbnary/tags</tagBase>
                    <useReleaseProfile>false</useReleaseProfile>
                    <preparationGoals>clean install</preparationGoals>
                    <goals>deploy</goals>
                    <arguments>-Prelease</arguments>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-site-plugin</artifactId>
                <configuration>
                    <outputEncoding>UTF-8</outputEncoding>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>native2ascii-maven-plugin</artifactId>
                <version>1.0-beta-1</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>native2ascii</goal>
                        </goals>
                        <configuration>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>2.2.1</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>net.alchim31.maven</groupId>
                    <artifactId>scala-maven-plugin</artifactId>
                    <version>3.2.0</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>3.1</version>
                    <!--version>2.0.2</version-->
                </plugin>
                <plugin>
                    <groupId>org.eclipse.m2e</groupId>
                    <artifactId>lifecycle-mapping</artifactId>
                    <version>1.0.0</version>
                    <configuration>
                        <lifecycleMappingMetadata>
                            <pluginExecutions>
                                <pluginExecution>
                                    <pluginExecutionFilter>
                                        <groupId>org.codehaus.mojo</groupId>
                                        <artifactId>native2ascii-maven-plugin</artifactId>
                                        <versionRange>[1.0-beta-1,)</versionRange>
                                        <goals>
                                            <goal>native2ascii</goal>
                                        </goals>
                                    </pluginExecutionFilter>
                                    <action>
                                        <execute />
                                    </action>
                                </pluginExecution>
                            </pluginExecutions>
                        </lifecycleMappingMetadata>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-site-plugin</artifactId>
                    <version>3.3</version>
                    <configuration>
                        <outputEncoding>UTF-8</outputEncoding>
                    </configuration>
                    <dependencies>
                        <dependency><!-- add support for ssh/scp -->
                            <groupId>org.apache.maven.wagon</groupId>
                            <artifactId>wagon-ssh-external</artifactId>
                            <version>1.0</version>
                        </dependency>
                    </dependencies>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>exec-maven-plugin</artifactId>
                    <version>1.2.1</version>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <repositories>
        <repository>
            <!-- Getalp third party stuff -->
            <id>getalp.repository</id>
            <url>http://getalp.imag.fr/m2/</url>
        </repository>
        <repository>
            <id>info-bliki-repository</id>
            <url>http://gwtwiki.googlecode.com/svn/maven-repository/</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>info-bliki-snapshot-repository</id>
            <url>http://gwtwiki.googlecode.com/svn/maven-snapshot-repository/</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>ukp-oss-releases</id>
            <url>http://zoidberg.ukp.informatik.tu-darmstadt.de/artifactory/public-releases</url>
            <releases>
                <enabled>true</enabled>
                <updatePolicy>never</updatePolicy>
                <checksumPolicy>warn</checksumPolicy>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>ukp-oss-snapshots</id>
            <url>http://zoidberg.ukp.informatik.tu-darmstadt.de/artifactory/public-snapshots</url>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>osr-public-releases</id>
            <name>OSR Public Releases</name>
            <url>http://mojo-maven.cs.fau.de/content/repositories/public-releases/</url>
        </repository>
        <repository>
            <id>osr-public-snapshots</id>
            <name>OSR Public snapshots</name>
            <url>http://mojo-maven.cs.fau.de/content/repositories/public-snapshots/</url>
        </repository>
        <repository>
            <id>sonatype-snapshots</id>
            <url>https://oss.sonatype.org/content/repositories/snapshots</url>
        </repository>
    </repositories>
</project>
